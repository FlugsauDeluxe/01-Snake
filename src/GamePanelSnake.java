import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Font;
import java.awt.FontMetrics;

import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.Timer;



public class GamePanelSnake extends JPanel {

    private final int width = 500;
    private final int height = 500;

    private SnakeModel model;
    private Timer timer;
    private boolean running = false;
    
 
 

    public GamePanelSnake() {

            this.setPreferredSize(this.getPreferredSize());
            model = new SnakeModel(this.getPreferredSize());
            setKeyBindings();
            setGUITimer();
    }

    private void setKeyBindings() {
        
        InputMap inputMap = getInputMap(WHEN_IN_FOCUSED_WINDOW);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "Left");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "Right");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0), "Up");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "Down");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Exit");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0), "spawnApple");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_E, 0), "extraTrail");
        
        ActionMap actionMap = getActionMap();
        actionMap.put("Left", new KeyPressedAction(model, "Left"));
        actionMap.put("Right", new KeyPressedAction(model, "Right"));
        actionMap.put("Up", new KeyPressedAction(model, "Up"));
        actionMap.put("Down", new KeyPressedAction(model, "Down"));
        actionMap.put("Exit", new ExitGame(model, "Exit"));
        actionMap.put("spawnApple", new ExitGame(model, "spawnApple"));
        actionMap.put("extraTrail", new ExitGame(model, "extraTrail"));
    }

    private void setGUITimer() {
        this.running = true;
        timer = new Timer(100, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                for (Asset asset : getModel().getAssets()) {
                    asset.update(GamePanelSnake.this);
                }
                repaint();
                if(model.getSnake().checkDeath()) {
                    running = false;
                    timer.stop();
                }
                model.checkApple();
            }
        });
        timer.start();
    }

    public SnakeModel getModel() {
        return model;
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(running) {
            Graphics2D g2d = (Graphics2D) g.create();
            for (Asset asset : getModel().getAssets()) {
                asset.paint(this, g2d);
            }
            g2d.dispose();
        } else {
            Font f = new Font("Calibri", Font.BOLD, 16);
            FontMetrics metrics = getFontMetrics(f);
            g.setFont(f);
            g.drawString("Game Over - You died, Bitch!",
             (width - metrics.stringWidth("Game Over - You died, Bitch!")) - 150, height/2);
        }
    }
}
