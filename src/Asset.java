import java.awt.Graphics2D;

public interface Asset {
    
    public void paint(GamePanelSnake surfacePlayground, Graphics2D g2d);

    public void update(GamePanelSnake surfacesurfacePlayground);

    public String getName();

}
