## Folder Structure

The workspace contains two folders by default, where:

- `bin`: compiled output files
- `src`: the folder to maintain sources
- `lib`: the folder to maintain dependencies
- `resources`:
