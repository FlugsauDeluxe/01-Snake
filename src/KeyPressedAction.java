import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;


public class KeyPressedAction extends AbstractAction {

    private SnakeModel snake;
    private String direction;




    public KeyPressedAction(SnakeModel snake, String direction) {
        this.snake = snake;
        this.direction = direction;
    }

    public SnakeModel getModel() {
        return snake;
    }

    public String getDirection() {
        return direction;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        snake.getSnake().setDirection(this.direction);
    }
    
}
