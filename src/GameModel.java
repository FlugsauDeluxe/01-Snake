public interface GameModel {
    
    public void add(Asset asset);

    public void remove(Asset asset);    

    public Iterable<Asset> getAssets();
    
}
