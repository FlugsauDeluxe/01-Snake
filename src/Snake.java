import java.awt.Rectangle;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Snake implements Asset {

    //grafische Attribute
    private BufferedImage[] headDirections = new BufferedImage[4];
    private BufferedImage head;
    private BufferedImage tail;

    //technische Attribute
    private int tailAmount = 3;
    private Rectangle[] snakeBounds;
    private Integer speedSnake = 1;
    private String direction = "Right";
    private Dimension playgroundSize;




    public Snake(Dimension playgroundSize) {
        try{
            //CAUTION it is supposed headDirection[i] == headDirection[j], i,j = 0,...,3
            headDirections[0] = ImageIO.read(getClass().getResource("headLeft.png"));
            headDirections[1] = ImageIO.read(getClass().getResource("headRight.png"));
            headDirections[2] = ImageIO.read(getClass().getResource("headUp.png"));
            headDirections[3] = ImageIO.read(getClass().getResource("headDown.png"));

            head = headDirections[1];

            tail = ImageIO.read(getClass().getResource("tail_v5.png"));
        } catch (IOException e){
        }

        int maxRectangle = ((int) playgroundSize.getWidth() * (int) playgroundSize.getHeight()) /
            (tail.getWidth() * tail.getHeight());

        snakeBounds = new Rectangle[maxRectangle];
        snakeBounds[0] = new Rectangle(head.getWidth(), head.getHeight());
        snakeBounds[0].x = (int) ((playgroundSize.getWidth() / 2) - (0 * tail.getWidth()));
        snakeBounds[0].y = (int) (playgroundSize.getHeight() / 2);

        //CAUTION it is supposed head.getWidth == tail.getWidth
        for(int i = 1; i < tailAmount; i++) {
            snakeBounds[i] = new Rectangle(tail.getWidth(), tail.getHeight());
            snakeBounds[i].x = (int) ((playgroundSize.getWidth() / 2) - (i * tail.getWidth()));
            snakeBounds[i].y = (int) (playgroundSize.getHeight() / 2);
        }

        this.playgroundSize = playgroundSize;
    }

    public Rectangle[] getBounds() {
        return this.snakeBounds;
    }

    public String getDirection() {
        return this.direction;
    }

    public void setDirection(String direction) {
        if(!(this.getDirection().equals(direction)) 
            && 
            (
             direction.equals("Left") ||
             direction.equals("Right") ||
             direction.equals("Up") ||
             direction.equals("Down")
            )
        ) {
            this.direction = direction;
        }
    }

    public boolean checkDeath() {
        if(checkCollisionWithTail() || checkCollisionWithWall()){
            return true;
        } else {
            return false;
        }
    }

    public boolean checkCollisionWithWall() {
        if  (   snakeBounds[0].x >= playgroundSize.width ||
                snakeBounds[0].x < 0 ||
                snakeBounds[0].y >= playgroundSize.height ||
                snakeBounds[0].y < 0
            ) 
            {
                return true;
            } else {
                return false;
            }
    }

    public boolean checkCollisionWithTail() {
        for(int i = tailAmount-1; i > 3; i--) {
            if  (snakeBounds[0].x == snakeBounds[i].x && snakeBounds[0].y == snakeBounds[i].y ) {
                return true;
            }
        }
        return false;
    }

    public void increaceTail() {
        snakeBounds[this.tailAmount] = new Rectangle(tail.getWidth(), tail.getHeight());
        snakeBounds[this.tailAmount].x = snakeBounds[this.tailAmount-1].x;
        snakeBounds[this.tailAmount].y = snakeBounds[this.tailAmount-1].y;
        this.tailAmount++;
    }

    @Override
    public void paint(GamePanelSnake surfacePlayground, Graphics2D g2d) {
        Rectangle[] bounds = this.getBounds();
        
        g2d.drawImage(head, bounds[0].x, bounds[0].y, surfacePlayground);
        for (int i = 1; i < tailAmount; i++) {
            g2d.drawImage(tail, bounds[i].x, bounds[i].y, surfacePlayground);
        }
    }

    @Override
    public void update(GamePanelSnake surfacesurfacePlayground) {
        moveSnakeTail();
        moveSnakeHead();
    }

    public void moveSnakeTail() {
        for(int i = tailAmount-1; i > 0; i--) {
            snakeBounds[i].x = snakeBounds[i-1].x;
            snakeBounds[i].y = snakeBounds[i-1].y;
        }
    }
    
    public void moveSnakeHead() {
        switch(this.direction) {
            case "Left":
                snakeBounds[0].x -= speedSnake * head.getWidth();
                head = headDirections[0];
                break;
            case "Right":
                snakeBounds[0].x += speedSnake * head.getWidth();
                head = headDirections[1];
                break;
            case "Up":
                snakeBounds[0].y -= speedSnake * head.getHeight();
                head = headDirections[2];
                break;
            case "Down":
                snakeBounds[0].y += speedSnake * head.getHeight();
                head = headDirections[3];
                break;
            default:
                System.out.println("Error wrong this.direction");
        }
    }

    @Override
    public String getName() {
        return "Snake";
    }
}
