import java.util.ArrayList;
import java.util.List;

import java.awt.Dimension;

public class SnakeModel implements GameModel{

    private Apple apple;
    private Snake snake;

    private List<Asset> assets;

    public SnakeModel(Dimension gamePanelSize) {
        apple = new Apple(gamePanelSize);
        snake = new Snake(gamePanelSize);

        assets = new ArrayList<Asset>(50);

    }

    public Snake getSnake() {
        return snake;
    }

    public Apple getAppel() {
        return apple;
    }

    public void checkApple() {
        int dif = 15;
        if  (   snake.getBounds()[0].x > apple.getBounds().x - dif &&
                snake.getBounds()[0].x < apple.getBounds().x + dif &&
                snake.getBounds()[0].y > apple.getBounds().y - dif &&
                snake.getBounds()[0].y < apple.getBounds().y + dif
            ) 
        {
            snake.increaceTail();
            apple.spawnApple();
       } 
    }
    // public void newApple() {
        // List<Asset> proxy = new ArrayList<>(assets);
        // proxy.remove(apple);
        // apple.spawnApple();
        // proxy.add(apple);        

    // }

    @Override
    public void add(Asset asset) {
        List<Asset> proxy = new ArrayList<>(assets);
        proxy.add(asset);
    }

    @Override
    public void remove(Asset asset) {
        assets.remove(asset);
    }    

    @Override
    public Iterable<Asset> getAssets() {
        List<Asset> proxy = new ArrayList<>(assets);
        proxy.add(apple);
        proxy.add(snake);
        return proxy;
    }
}
