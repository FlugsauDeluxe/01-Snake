import java.awt.EventQueue;

import javax.swing.JFrame;

public class Main {

    
    public static void main(String[] arg0) {
        EventQueue.invokeLater(new Runnable() {
            
            @Override
            public void run() {
                JFrame frame = new JFrame("Moving Apple");
                frame.setResizable(false);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                frame.add(new GamePanelSnake());
                frame.setFocusable(true);
                frame.pack();
                frame.setLocationRelativeTo(null);
                frame.setVisible(true);                
            }
        });
    }

    public static int randomIntWithRange(int min, int max) {
        int range = (max - min) + 1;
        return (int) (Math.random() * range) + min;
    }
}