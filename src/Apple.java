import java.io.IOException;

import java.awt.image.BufferedImage;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import javax.imageio.ImageIO;



public class Apple implements Asset {

    //grafische Attribute
    private BufferedImage apple;

    //technische Attribute
    private Rectangle bounds;
    private Dimension gamePanelSize;




    public Apple(Dimension gamePanelSize) {
        this.gamePanelSize = gamePanelSize;
        setApple();
    }

    private void setApple() {
        try {
            apple = ImageIO.read(getClass().getResource("apple_v5.png"));
        } catch (IOException e){
        }
        bounds = new Rectangle();
        spawnApple();
        bounds.setSize(apple.getWidth(), apple.getHeight());
    }

    public void spawnApple() { 
        bounds.x = Main.randomIntWithRange(0, (int) gamePanelSize.getWidth() - 10);
        bounds.y = Main.randomIntWithRange(0, (int) gamePanelSize.getHeight() - 10);
    }

    public Rectangle getBounds() {
        return this.bounds;
    }        

    @Override
    public void paint(GamePanelSnake surfacePlayground, Graphics2D g2d) {
        Rectangle bounds = this.getBounds();
        g2d.drawImage(apple, bounds.x, bounds.y, surfacePlayground);
    }

    @Override
    public void update(GamePanelSnake surfacesurfacePlayground) {
    }

    @Override
    public String getName() {
        return "Apple";
    }    
}
