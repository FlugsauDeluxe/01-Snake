import java.awt.Dimension;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

public class ExitGame extends AbstractAction {

    private SnakeModel model;
    private String modus;
    

    ExitGame(SnakeModel model, String modus) {
        this.model = model;
        this.modus = modus;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if(modus.equals("Exit")) {
            System.exit(0);
        }
        if(modus.equals("spawnApple")) {
            model.getAppel().spawnApple();
        }
        if(modus.equals("extraTrail")) {
            model.getSnake().increaceTail();
        }
        
    }

}
